package com.project.studyjavaweb.service.services.exception;

public class AnswerUpdateEx extends RuntimeException {
	
	public AnswerUpdateEx(String message) {
		super(message);
	}
}
