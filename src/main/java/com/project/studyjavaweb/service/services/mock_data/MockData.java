package com.project.studyjavaweb.service.services.mock_data;

import com.project.studyjavaweb.repository.entity.AnswerEntity;
import com.project.studyjavaweb.repository.entity.DifficultyEntity;
import com.project.studyjavaweb.repository.entity.ProfileEntity;
import com.project.studyjavaweb.repository.entity.QuestionEntity;
import com.project.studyjavaweb.repository.repository.AnswerRepository;
import com.project.studyjavaweb.repository.repository.DifficultyRepository;
import com.project.studyjavaweb.repository.repository.ProfileRepository;
import com.project.studyjavaweb.repository.repository.QuestionRepository;

import org.springframework.stereotype.Service;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class MockData {
	
	private final AnswerRepository answerService;
	private final QuestionRepository questionService;
	private final DifficultyRepository difficultyService;
	private final ProfileRepository profileService;
	
	private void mockData() {
		DifficultyEntity difficulty1 = difficultyService.save(new DifficultyEntity(null, "HARD"));
		DifficultyEntity difficulty2 = difficultyService.save(new DifficultyEntity(null, "MEDIUM"));
		difficultyService.save(new DifficultyEntity(null, "EASY"));
		
		ProfileEntity profile1 = profileService.save(new ProfileEntity(null, "PROFILE_1"));
		ProfileEntity profile2 = profileService.save(new ProfileEntity(null, "PROFILE_2"));
		profileService.save(new ProfileEntity(null, "PROFILE_3"));
		
		QuestionEntity question1 = questionService.save(new QuestionEntity(null, "QUESTION_1"));
		questionService.save(new QuestionEntity(null, "QUESTION_2"));
		questionService.save(new QuestionEntity(null, "QUESTION_3"));
		
		answerService.save(new AnswerEntity(null,
											"ANSWER_1",
											question1,
											difficulty1,
											profile1));
		answerService.save(new AnswerEntity(null,
											"ANSWER_2",
											question1,
											difficulty2,
											profile2));
	}
	
	@PostConstruct
	private void postBuild() {
		mockData();
	}
}
