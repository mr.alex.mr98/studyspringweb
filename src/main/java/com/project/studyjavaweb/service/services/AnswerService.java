package com.project.studyjavaweb.service.services;

import com.project.studyjavaweb.repository.repository.AnswerRepository;
import com.project.studyjavaweb.service.dto.AnswerDTO;
import com.project.studyjavaweb.service.mappers.AnswerMapper;
import com.project.studyjavaweb.service.services.exception.AnswerNotFoundEx;
import com.project.studyjavaweb.service.services.exception.AnswerSaveEx;
import com.project.studyjavaweb.service.services.exception.AnswerUpdateEx;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class AnswerService {
	
	private final AnswerRepository repository;
	private final AnswerMapper mapper;
	
	@Transactional
	public void save(final AnswerDTO dto) {
		try {
			repository.save(mapper.toEntity(dto));
		} catch (Exception ex) {
			throw new AnswerSaveEx("Error while saving the answer!");
		}
	}
	
	@Transactional
	public void delete(final Long id) {
		repository.deleteById(id);
	}
	
	public AnswerDTO getById(final Long id) {
		return repository.findById(id)
						 .map(mapper::toDto)
						 .orElseThrow(() -> new AnswerNotFoundEx("Answer has not been found!"));
	}
	
	public List<AnswerDTO> getAll() {
		return repository.findAll()
						 .stream()
						 .map(mapper::toDto)
						 .collect(Collectors.toList());
	}
	
	@Transactional
	public Integer update(final AnswerDTO answerDTO) {
		return Optional.ofNullable(repository.update(answerDTO.getId(), answerDTO.getAnswer()))
					   .filter(i -> i > 0)
					   .orElseThrow(() -> new AnswerUpdateEx("Error while updating the answer!"));
	}
}
