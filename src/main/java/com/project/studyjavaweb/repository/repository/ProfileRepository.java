package com.project.studyjavaweb.repository.repository;

import com.project.studyjavaweb.repository.entity.ProfileEntity;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ProfileRepository extends JpaRepository<ProfileEntity, Long> {
}
