package com.project.studyjavaweb.service.mappers;

import com.project.studyjavaweb.repository.entity.ProfileEntity;
import com.project.studyjavaweb.service.dto.ProfileDTO;

import org.mapstruct.ReportingPolicy;


@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR,
					  componentModel = "spring")
public interface ProfileMapper {
	
	ProfileEntity toEntity(ProfileDTO profileDTO);
	ProfileDTO toDto(ProfileEntity profileEntity);
}