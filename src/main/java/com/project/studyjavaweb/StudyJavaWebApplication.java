package com.project.studyjavaweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StudyJavaWebApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(StudyJavaWebApplication.class, args);
	}
}
