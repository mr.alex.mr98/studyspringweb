package com.project.studyjavaweb.controller.ex_handler;

import com.project.studyjavaweb.service.services.exception.AnswerNotFoundEx;
import com.project.studyjavaweb.service.services.exception.AnswerSaveEx;
import com.project.studyjavaweb.service.services.exception.AnswerUpdateEx;
import com.project.studyjavaweb.service.services.exception.NullArgumentEx;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.time.LocalDateTime;

import lombok.extern.slf4j.Slf4j;


@ControllerAdvice
@Slf4j
public class ExceptionHandler {
	
	@org.springframework.web.bind.annotation.ExceptionHandler({AnswerNotFoundEx.class, AnswerUpdateEx.class, AnswerSaveEx.class, NullArgumentEx.class})
	protected ResponseEntity<Object> handleConflict(RuntimeException ex) {
		log.error("error - {}, time - {}, stackTrace - {}", ex.getMessage(), LocalDateTime.now(), ex.getStackTrace());
		return new ResponseEntity<>(ex.getMessage(), HttpStatusCode.valueOf(400));
	}
}
