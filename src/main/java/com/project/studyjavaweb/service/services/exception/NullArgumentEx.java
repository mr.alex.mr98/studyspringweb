package com.project.studyjavaweb.service.services.exception;

public class NullArgumentEx extends RuntimeException {
	
	public NullArgumentEx(String message) {
		super(message);
	}
}
