package com.project.studyjavaweb.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerDTO {
	
	private Long id;
	private String answer;
	private QuestionDTO questionDTO;
	private DifficultyDTO difficultyDTO;
	private ProfileDTO profileDTO;
}
