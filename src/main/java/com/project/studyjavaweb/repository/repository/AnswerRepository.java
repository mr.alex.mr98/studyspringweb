package com.project.studyjavaweb.repository.repository;

import com.project.studyjavaweb.repository.entity.AnswerEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface AnswerRepository extends JpaRepository<AnswerEntity, Long> {
	
	@Modifying
	@Query("update AnswerEntity ae " +
		   "set ae.answer = :answer " +
		   "where ae.id = :id")
	Integer update(@Param("id") Long id,
				@Param("answer") String answer);
}
