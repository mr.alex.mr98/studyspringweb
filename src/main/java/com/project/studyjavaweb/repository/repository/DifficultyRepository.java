package com.project.studyjavaweb.repository.repository;

import com.project.studyjavaweb.repository.entity.DifficultyEntity;

import org.springframework.data.jpa.repository.JpaRepository;


public interface DifficultyRepository extends JpaRepository<DifficultyEntity, Long> {
}
