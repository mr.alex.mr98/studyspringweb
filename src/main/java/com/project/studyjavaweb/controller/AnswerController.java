package com.project.studyjavaweb.controller;

import com.project.studyjavaweb.service.dto.AnswerDTO;
import com.project.studyjavaweb.service.services.AnswerService;
import com.project.studyjavaweb.service.services.exception.NullArgumentEx;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping(AnswerController.ANSWERS_API)
@RequiredArgsConstructor
@Slf4j
public class AnswerController {
	
	private final AnswerService answerService;
	
	public static final String ANSWERS_API = "/answers";
	
	@PostMapping
	public ResponseEntity<String> save(@RequestBody final AnswerDTO dto) {
		if (dto == null) {
			throw new NullArgumentEx("Argument can not be null!");
		}
		answerService.save(dto);
		log.info("Saving: answer - {}, question - {}, difficulty - {}, profile - {}",
				 dto.getAnswer(), dto.getQuestionDTO(), dto.getDifficultyDTO(), dto.getProfileDTO());
		return ResponseEntity.ok("The answer has been saved");
	}
	
	@DeleteMapping
	public ResponseEntity<String> delete(@RequestParam final Long id) {
		if (id == null) {
			throw new NullArgumentEx("Argument can not be null!");
		}
		answerService.delete(id);
		log.info("Deleting: answer id - {}", id);
		return ResponseEntity.ok("The answer has been deleted");
	}
	
	@GetMapping
	public ResponseEntity<AnswerDTO> getById(@RequestParam final Long id) {
		return Optional.ofNullable(id)
					   .map(i -> ResponseEntity.ok(answerService.getById(i)))
					   .orElseThrow(() -> new NullArgumentEx("Argument can not be null!"));
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<AnswerDTO>> getAll() {
		return ResponseEntity.ok(answerService.getAll());
	}
	
	@PatchMapping
	public ResponseEntity<String> update(@RequestBody final AnswerDTO dto) {
		final ResponseEntity<String> answer = Optional.ofNullable(dto)
													  .map(i -> {
														  ResponseEntity.ok(String.valueOf(answerService.update(i)));
														  return ResponseEntity.ok("The answer has been updated");
													  })
													  .orElseThrow(() -> new NullArgumentEx("Argument can not be null!"));
		log.info("Updating: answer id - {}, new answer - {}", dto.getId(), dto.getAnswer());
		return answer;
	}
}
