package com.project.studyjavaweb.repository.repository;

import com.project.studyjavaweb.repository.entity.QuestionEntity;

import org.springframework.data.jpa.repository.JpaRepository;


public interface QuestionRepository extends JpaRepository<QuestionEntity, Long> {
}
