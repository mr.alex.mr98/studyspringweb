package com.project.studyjavaweb.service.mappers;

import com.project.studyjavaweb.repository.entity.QuestionEntity;
import com.project.studyjavaweb.service.dto.QuestionDTO;

import org.mapstruct.ReportingPolicy;


@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR,
					  componentModel = "spring")
public interface QuestionMapper {
	
	QuestionEntity toEntity(QuestionDTO questionDTO);
	QuestionDTO toDto(QuestionEntity questionEntity);
}