package com.project.studyjavaweb.service.services.exception;

public class AnswerSaveEx extends RuntimeException {
	
	public AnswerSaveEx(String message) {
		super(message);
	}
}
