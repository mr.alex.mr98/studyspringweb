package com.project.studyjavaweb.service.services.exception;

public class AnswerNotFoundEx extends RuntimeException {
	
	public AnswerNotFoundEx(String message) {
		super(message);
	}
}
