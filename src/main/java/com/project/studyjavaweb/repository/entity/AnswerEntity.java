package com.project.studyjavaweb.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Table(name = "answer")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AnswerEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String answer;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private QuestionEntity questionEntity;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "difficulty_id")
	private DifficultyEntity difficultyEntity;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id")
	private ProfileEntity profileEntity;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AnswerEntity that = (AnswerEntity) o;
		return id != null && id.equals(that.id);
	}
	
	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
