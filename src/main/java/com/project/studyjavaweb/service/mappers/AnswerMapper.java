package com.project.studyjavaweb.service.mappers;

import com.project.studyjavaweb.repository.entity.AnswerEntity;
import com.project.studyjavaweb.service.dto.AnswerDTO;

import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;


@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR,
					  componentModel = "spring",
					  uses = {DifficultyMapper.class, ProfileMapper.class, QuestionMapper.class})
public interface AnswerMapper {
	
	@Mapping(target = "questionEntity", source = "questionDTO")
	@Mapping(target = "profileEntity", source = "profileDTO")
	@Mapping(target = "difficultyEntity", source = "difficultyDTO")
	AnswerEntity toEntity(AnswerDTO answerDTO);
	@Mapping(target = "questionDTO", source = "questionEntity")
	@Mapping(target = "profileDTO", source = "profileEntity")
	@Mapping(target = "difficultyDTO", source = "difficultyEntity")
	AnswerDTO toDto(AnswerEntity answerEntity);
}