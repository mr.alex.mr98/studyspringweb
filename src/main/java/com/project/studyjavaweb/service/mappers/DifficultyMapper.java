package com.project.studyjavaweb.service.mappers;

import com.project.studyjavaweb.repository.entity.DifficultyEntity;
import com.project.studyjavaweb.service.dto.DifficultyDTO;

import org.mapstruct.ReportingPolicy;


@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR,
					  componentModel = "spring")
public interface DifficultyMapper {
	
	DifficultyEntity toEntity(DifficultyDTO difficultyDTO);
	DifficultyDTO toDto(DifficultyEntity difficultyEntity);
}